# Copyright 2024 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from __future__ import annotations

import datetime as dt
import json
import logging
from typing import TYPE_CHECKING, Any, Dict, Optional, Sequence, Tuple, Type

import selenium.common.exceptions
import urllib3.exceptions

from crossbench import helper
from crossbench.action_runner.action_runner_listener import \
    ActionRunnerListener
from crossbench.action_runner.basic_action_runner import BasicActionRunner
from crossbench.benchmarks.base import (BenchmarkProbeMixin, StoryFilter,
                                        SubStoryBenchmark)
from crossbench.benchmarks.loading.page.base import Page
from crossbench.benchmarks.loading.page.live import LivePage
from crossbench.benchmarks.loading.tab_controller import TabController
from crossbench.parse import NumberParser
from crossbench.probes.json import JsonResultProbe, JsonResultProbeContext
from crossbench.probes.metric import MetricsMerger
from crossbench.probes.results import ProbeResult, ProbeResultDict
from crossbench.runner.exception import StopStoryException

if TYPE_CHECKING:
  import argparse

  from crossbench.action_runner.base import ActionRunner
  from crossbench.cli.parser import CrossBenchArgumentParser
  from crossbench.path import LocalPath
  from crossbench.runner.actions import Actions
  from crossbench.runner.groups.browsers import BrowsersRunGroup
  from crossbench.runner.groups.stories import StoriesRunGroup
  from crossbench.runner.run import Run


class MemoryProbe(BenchmarkProbeMixin, JsonResultProbe):
  """
  Memory-specific Probe.
  Extracts the number of alive tabs.
  """
  NAME: str = "memory_probe"

  def get_context(self, run: Run) -> MemoryProbeContext:
    return MemoryProbeContext(self, run)

  def to_json(self, actions: Actions) -> Dict[str, float]:
    raise NotImplementedError(
        "should not be called, data comes from memory probe context")

  def log_run_result(self, run: Run) -> None:
    self._log_result(run.results, single_result=True)

  def log_browsers_result(self, group: BrowsersRunGroup) -> None:
    self._log_result(group.results, single_result=False)

  def _log_result(self, result_dict: ProbeResultDict,
                  single_result: bool) -> None:

    if self not in result_dict:
      return
    results_json: LocalPath = result_dict[self].json
    logging.info("-" * 80)
    logging.critical("Memory results (num of alive tabs):")
    if not single_result:
      logging.critical("  %s", result_dict[self].csv)
    logging.info("- " * 40)

    with results_json.open(encoding="utf-8") as f:
      data = json.load(f)
      if single_result:
        logging.critical("Score %s", data["alive_tab_count"])
      else:
        self._log_result_metrics(data)

  def merge_stories(self, group: StoriesRunGroup) -> ProbeResult:
    merged = MetricsMerger.merge_json_list(
        repetitions_group.results[self].json
        for repetitions_group in group.repetitions_groups)
    return self.write_group_result(group, merged)

  def merge_browsers(self, group: BrowsersRunGroup) -> ProbeResult:
    return self.merge_browsers_json_list(group).merge(
        self.merge_browsers_csv_list(group))


class MemoryProbeContext(ActionRunnerListener,
                         JsonResultProbeContext[MemoryProbe]):

  def __init__(self, probe: MemoryProbe, run: Run) -> None:
    super().__init__(probe, run)
    cur_benchmark = probe.benchmark
    if not isinstance(cur_benchmark, MemoryBenchmark):
      raise TypeError("The probe only works for MemoryBenchmark")
    cur_benchmark.action_runner.set_listener(self)
    self._skippable_tab_count = cur_benchmark._skippable_tab_count
    # Records the navigation_start_time time for each window handle.
    self._navigation_time_ms: Dict[str, float] = {}
    self._tab_count: int = 1

  def start(self) -> None:
    pass

  def to_json(self, actions: Actions) -> Dict[str, int]:
    return {"alive_tab_count": self._tab_count - 1}

  def _increment_tab_count(self):
    self._tab_count += 1

  def _record_navigation_time(self, run: Run) -> None:
    """
    Record NavigationStart time for each handle.
    """
    with run.actions("_record_navigation_time", measure=False) as action:
      cur_handle: str = action.current_window_id()
      navigation_start_time = action.js(
          "return window.performance.timing.navigationStart")
      logging.debug("Browser: %s. Navigation starttime for handle %s is %s.",
                    run.browser.unique_name, cur_handle, navigation_start_time)
      self._navigation_time_ms[cur_handle] = navigation_start_time

  def _check_liveness(self, run: Run) -> None:
    """
    Navigate each opened tab, and check if the navigation start time
    has changed. If so, then it means that page has been discarded
    and reloaded.
    """
    with run.actions("_check_liveness", measure=False) as action:
      for handle, handle_navigation_time_ms in self._navigation_time_ms.items():
        logging.debug("Browser: %s. Liveness checking for handle: %s",
                      run.browser, handle)
        action.switch_window(handle)
        action.wait(1)
        navigation_start_time = action.js(
            "return window.performance.timing.navigationStart")
        if navigation_start_time != handle_navigation_time_ms:
          logging.info(
              "Browser: %s. The max num of tabs we can keep alive concurrently "
              "is: %s ", run.browser, self._tab_count - 1)
          raise StopStoryException("Found a page that has been reloaded.")

  def _check_error_msg(self, e: Exception):
    if isinstance(e, selenium.common.exceptions.WebDriverException
                 ) and "page crash" in str(e):
      return True
    if isinstance(e, selenium.common.exceptions.TimeoutException):
      return True
    if isinstance(e, urllib3.exceptions.ReadTimeoutError):
      return True
    # Error msg from `Could not execute JS` due to page crash.
    if isinstance(e, ValueError) and "page crash" in str(e):
      return True
    return False

  def handle_error(self, run: Run, e: Exception) -> None:
    """
    If there is a page crash error or a http request time out
    for the stress liveness test, directly exit the benchmark
    and report the max alive tab count.
    """
    if self._check_error_msg(e):
      logging.info(
          "Browser: %s. The max num of tabs we can keep alive concurrently "
          "is: %s ", run.browser, self._tab_count - 1)
      raise StopStoryException(f"Found a Tab Crash/Timeout: {e}")

  def handle_page_run(self, run: Run) -> None:
    self._record_navigation_time(run)
    if self._tab_count > self._skippable_tab_count:
      self._check_liveness(run)

  def handle_new_tab(self, run: Run) -> None:
    self._increment_tab_count()


class MemoryBenchmarkStoryFilter(StoryFilter[Page]):
  """
  Create memory story
  Specify alloc-count, block-size, compressiblity,
  prefill-constnat, random style to decide the
  memory workload.
  """
  stories: Sequence[Page]
  URL = "https://chromium-workloads.web.app/web-tests/main/synthetic/memory"

  @classmethod
  def add_cli_parser(
      cls, parser: argparse.ArgumentParser) -> argparse.ArgumentParser:
    parser = super().add_cli_parser(parser)
    parser.add_argument(
        "--alloc-count",
        type=NumberParser.positive_int,
        default=1,
        help="The number of block to allocate.")
    parser.add_argument(
        "--block-size",
        type=NumberParser.positive_int,
        default=128,
        help="The size of each block (MB).")
    parser.add_argument(
        "--compressibility",
        type=NumberParser.positive_zero_int,
        default=0,
        help="The compressibility (0-100)")
    parser.add_argument(
        "--prefill-constant",
        type=NumberParser.any_int,
        default=1,
        help="Prefill memory buffer with given constant (-1-127)."
        "Default is 1."
        "-1 represents no prefilling.")
    parser.add_argument(
        "--random-per-buffer",
        dest="random_per_page",
        action="store_false",
        help="With the flag, it will generate the memory workload "
        "with random per buffer level. Without the flag,"
        "it will generate the memory workload with random"
        "per page level.")

    tab_group = parser.add_mutually_exclusive_group()
    tab_group.add_argument(
        "--tabs",
        type=TabController.parse,
        default=TabController.default(),
        help="Open memory workload in single/multiple/infinity tabs. "
        "Default is single."
        "Valid values are: 'single', 'inf', 'infinity', number")
    tab_group.add_argument(
        "--single-tab",
        dest="tabs",
        const=TabController.single(),
        default=TabController.default(),
        action="store_const",
        help="Open memory workload in a single tab."
        "Equivalent to --tabs=single")
    tab_group.add_argument(
        "--infinite-tab",
        dest="tabs",
        const=TabController.forever(),
        action="store_const",
        help="Open memory workload in separate tabs infinitely."
        "Equivalent to --tabs=infinity")
    return parser

  @classmethod
  def kwargs_from_cli(cls, args: argparse.Namespace) -> Dict[str, Any]:
    kwargs = super().kwargs_from_cli(args)
    kwargs["args"] = args
    return kwargs

  def __init__(self,
               story_cls: Type[Page],
               patterns: Sequence[str],
               args: argparse.Namespace,
               separate: bool = False) -> None:
    self._args: argparse.Namespace = args

    super().__init__(story_cls, patterns, separate)

  def process_all(self, patterns: Sequence[str]) -> None:
    self.stories = self.stories_from_cli_args(self._args)

  @classmethod
  def stories_from_cli_args(cls, args: argparse.Namespace) -> Sequence[Page]:
    url_params = {
        "alloc": str(args.alloc_count),
        "blocksize": str(args.block_size),
        "compress": str(args.compressibility),
        "prefill": str(args.prefill_constant),
    }
    if not args.random_per_page:
      url_params["randomperpage"] = "false"
    url = helper.update_url_query(cls.URL, url_params)
    stories: Sequence[Page] = []
    page = LivePage("memory", url, dt.timedelta(seconds=2), tabs=args.tabs)
    stories = [page]
    return stories

  def create_stories(self, separate: bool) -> Sequence[Page]:
    logging.info("SELECTED STORIES: %s", ", ".join(map(str, self.stories)))
    return self.stories


class MemoryBenchmark(SubStoryBenchmark):
  """
  Benchmark runner for memory stress test.
  """

  NAME = "memory"
  DEFAULT_STORY_CLS = Page
  STORY_FILTER_CLS = MemoryBenchmarkStoryFilter
  PROBES: Tuple[Type[MemoryProbe], ...] = (MemoryProbe,)

  @classmethod
  def add_cli_parser(
      cls, subparsers: argparse.ArgumentParser, aliases: Sequence[str] = ()
  ) -> CrossBenchArgumentParser:
    parser = super().add_cli_parser(subparsers, aliases)
    cls.STORY_FILTER_CLS.add_cli_parser(parser)
    parser.add_argument(
        "--skippable-tab-count",
        type=NumberParser.positive_int,
        default=0,
        help="The number of tabs that can be skipped for liveness checking.")
    return parser

  @classmethod
  def kwargs_from_cli(cls, args: argparse.Namespace) -> Dict[str, Any]:
    kwargs = super().kwargs_from_cli(args)
    kwargs["skippable_tab_count"] = args.skippable_tab_count
    return kwargs

  @classmethod
  def stories_from_cli_args(cls, args: argparse.Namespace) -> Sequence[Page]:
    super().stories_from_cli_args(args)
    stories = MemoryBenchmarkStoryFilter.stories_from_cli_args(args)
    return stories

  @classmethod
  def all_story_names(cls) -> Tuple[str, ...]:
    return ()

  def __init__(self,
               stories: Sequence[Page],
               skippable_tab_count: int = 0,
               action_runner: Optional[ActionRunner] = None) -> None:
    self._action_runner = action_runner or BasicActionRunner()
    for story in stories:
      assert isinstance(story, Page)
    super().__init__(stories)
    self._skippable_tab_count = skippable_tab_count

  @classmethod
  def describe(cls) -> Dict[str, Any]:
    data = super().describe()
    data["url"] = cls.STORY_FILTER_CLS.URL
    return data

  @property
  def action_runner(self) -> ActionRunner:
    return self._action_runner
