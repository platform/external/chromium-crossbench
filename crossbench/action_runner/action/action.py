# Copyright 2024 The Chromium Authors
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from __future__ import annotations

import abc
import datetime as dt
from typing import TYPE_CHECKING, Any, Dict, Type, TypeVar

from crossbench import exception
from crossbench.action_runner.action.action_type import ActionType
from crossbench.config import ConfigObject, ConfigParser
from crossbench.parse import DurationParser, NumberParser, ObjectParser

if TYPE_CHECKING:
  from crossbench.action_runner.base import ActionRunner
  from crossbench.runner.run import Run
  from crossbench.types import JsonDict


class ActionTypeConfigParser(ConfigParser):
  """Custom ConfigParser for ActionType that works on
  Action Configs. This way we can pop the 'value' or 'type' key from the
  config dict."""

  def __init__(self):
    super().__init__("ActionType parser", ActionType)
    self.add_argument(
        "action",
        aliases=("type",),
        type=ObjectParser.non_empty_str,
        required=True)

  def new_instance_from_kwargs(self, kwargs: Dict[str, Any]) -> ActionType:
    return ActionType(kwargs["action"])


_ACTION_TYPE_CONFIG_PARSER = ActionTypeConfigParser()

ACTION_TIMEOUT = dt.timedelta(seconds=20)

ActionT = TypeVar("ActionT", bound="Action")

# Lazily initialized Action class lookup.
ACTIONS: Dict[ActionType, Type[Action]] = {}


class Action(ConfigObject, metaclass=abc.ABCMeta):
  TYPE: ActionType = ActionType.GET

  @classmethod
  def parse_str(cls, value: str) -> Action:
    return ACTIONS[ActionType.GET].parse_str(value)

  @classmethod
  def parse_dict(cls: Type[ActionT], config: Dict[str, Any]) -> ActionT:
    action_type: ActionType = _ACTION_TYPE_CONFIG_PARSER.parse(config)
    action_cls: Type[ActionT] = ACTIONS[action_type]
    with exception.annotate_argparsing(
        f"Parsing Action details  ...{{ action: \"{action_type}\", ...}}:"):
      action = action_cls.config_parser().parse(config)
    assert isinstance(action, cls), f"Expected {cls} but got {type(action)}"
    return action

  @classmethod
  def config_parser(cls: Type[ActionT]) -> ConfigParser[ActionT]:
    parser = ConfigParser(f"{cls.__name__} parser", cls)
    parser.add_argument(
        "index", type=NumberParser.positive_zero_int, required=False, default=0)
    parser.add_argument(
        "timeout",
        type=DurationParser.positive_duration,
        default=ACTION_TIMEOUT)
    return parser

  def __init__(self, timeout: dt.timedelta = ACTION_TIMEOUT, index: int = 0):
    self._timeout: dt.timedelta = timeout
    self._index = index
    self.validate()

  @property
  def index(self) -> int:
    return self._index

  @property
  def duration(self) -> dt.timedelta:
    return dt.timedelta(milliseconds=10)

  @property
  def timeout(self) -> dt.timedelta:
    return self._timeout

  @property
  def has_timeout(self) -> bool:
    return self._timeout != dt.timedelta.max

  @abc.abstractmethod
  def run_with(self, run: Run, action_runner: ActionRunner) -> None:
    pass

  def validate(self) -> None:
    if self._timeout.total_seconds() < 0:
      raise ValueError(
          f"{self}.timeout should be positive, but got {self.timeout}")

  def to_json(self) -> JsonDict:
    return {"type": str(self.TYPE), "timeout": self.timeout.total_seconds()}

  def __str__(self) -> str:
    return type(self).__name__

  def __eq__(self, other: object) -> bool:
    if isinstance(other, Action):
      return self.to_json() == other.to_json()
    return False
